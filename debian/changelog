libterm-progressbar-perl (2.23-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.1.5, no changes needed.

  [ Salvatore Bonaccorso ]
  * Import upstream version 2.23.
  * Annotate test-only build dependencies with <!nocheck>
  * Declare compliance with Debian policy 4.6.1
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 09 Oct 2022 09:55:57 +0200

libterm-progressbar-perl (2.22-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org
  * Import upstream version 2.22
  * Add Build-Depends-Indep on libtest-warnings-perl
  * Set Rules-Requires-Root to no
  * Bump Debhelper compat level to 10
  * Declare compliance with Debian policy 4.1.4
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 12 May 2018 17:01:23 +0200

libterm-progressbar-perl (2.21-1) unstable; urgency=medium

  * Import upstream version 2.21

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 02 Aug 2017 21:36:16 +0200

libterm-progressbar-perl (2.20-1) unstable; urgency=medium

  * Import upstream version 2.20
  * Update copyright years for debian/* packaging files
  * Declare compliance with Debian policy 4.0.0

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 13 Jul 2017 15:21:36 +0200

libterm-progressbar-perl (2.18-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Import upstream version 2.18
  * Debian metadata: update upstream repository URL
  * Bump Debhelper compat level to 9
  * debian/rules: Drop explicit setting of buildsystem to perl_makemaker
  * Update copyright years for debian/* packaging files
  * Make Build-Depends-Indep on libtest-exception-perl unversioned
  * Make Build-Depends-Indep on libcapture-tiny-perl unversioned
  * Run wrap-and-sort -ast on debian/control
  * Declare compliance with Debian policy 3.9.8

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 06 Dec 2016 22:31:28 +0100

libterm-progressbar-perl (2.17-3) unstable; urgency=medium

  * Team upload.
  * Fix autopkgtest failures by running prove without the --merge option.
    (Closes: #785045)

 -- Niko Tyni <ntyni@debian.org>  Sat, 26 Sep 2015 23:04:41 +0300

libterm-progressbar-perl (2.17-2) unstable; urgency=medium

  * Upload to unstable

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 28 Apr 2015 18:17:54 +0200

libterm-progressbar-perl (2.17-1) experimental; urgency=medium

  * Imported upstream version 2.17
  * Update copyright years for debian/* packaging files
  * Declare compliance with Debian Policy 3.9.6
  * Add Testsuite: autopkgtest-pkg-perl field in debian/control

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 08 Mar 2015 17:59:16 +0100

libterm-progressbar-perl (2.16-1) unstable; urgency=medium

  * Update Vcs-Browser URL to cgit web frontend
  * Add debian/upstream/metadata
  * Imported upstream version 2.16

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 09 Sep 2014 17:51:38 +0200

libterm-progressbar-perl (2.15-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 2.15
  * Update copyright years for debian/* packaging files
  * Declare compliance with Debian Policy 3.9.5

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 14 Apr 2014 08:16:20 +0200

libterm-progressbar-perl (2.14-1) unstable; urgency=low

  * Imported Upstream version 2.14
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs
  * Bump Standards-Version 3.9.4
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 21 Jul 2013 20:44:28 +0200

libterm-progressbar-perl (2.13-1) unstable; urgency=low

  * Imported Upstream version 2.13
  * Add myself to Uploaders and debian/copyright
  * Bump Standards-Version to 3.9.3
  * Update format of debian/copyright file.
    Update format to copyright-format 1.0 as released together with Debian
    policy 3.9.3.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 19 May 2012 23:18:02 +0200

libterm-progressbar-perl (2.11-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 2.11
  * Drop 01_tests.diff patch.
    Upstream now skips the signature verification.
  * Refer to Debian systems instead of only Debian GNU/Linux systems

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 18 Feb 2012 11:00:25 +0100

libterm-progressbar-perl (2.10-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * debian/rules: use perl_makemaker (Build.PL is gone).
  * Update years of upstream and packaging copyright.
  * Bump Standards-Version to 3.9.2 (no changes).
  * Switch to debhelper compatibility level 8.
  * New build dependencies: libtest-exception-perl, libcapture-tiny-perl.

 -- gregor herrmann <gregoa@debian.org>  Thu, 22 Dec 2011 19:54:25 +0100

libterm-progressbar-perl (2.09-6) unstable; urgency=low

  [ Ryan Niebur ]
  * moved with permission from Bart (Closes: #531507)
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza);
    ${misc:Depends} to Depends: field. Removed: Homepage pseudo-field
    (Description). Changed: Maintainer set to Debian Perl Group <pkg-
    perl-maintainers@lists.alioth.debian.org> (was: Bart Martens
    <bartm@knars.be>); Bart Martens <bartm@knars.be> moved to Uploaders.
  * debian/watch: use dist-based URL.
  * remove Bart from Uploaders

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ gregor herrmann ]
  * Convert to source format 3.0 (quilt).
  * Add /me to Uploaders.
  * Set Standards-Version to 3.9.1 (no changes).
  * debhelper 7.
  * debian/copyright. DEP5 format.
  * Refresh patch.

 -- gregor herrmann <gregoa@debian.org>  Tue, 27 Jul 2010 20:39:39 -0400

libterm-progressbar-perl (2.09-5) unstable; urgency=low

  * debian/control, debian/rules: Repackaged with cdbs.
  * debian/copyright: Updated.
  * debian/examples: Added.
  * debian/patches/01_tests.diff: Added.
  * debian/watch: Updated.

 -- Bart Martens <bartm@knars.be>  Wed, 16 May 2007 19:52:57 +0200

libterm-progressbar-perl (2.09-4) unstable; urgency=low

  * New maintainer.  Closes: #344457.
  * debian/control: Removed version from homepage URL.

 -- Bart Martens <bartm@knars.be>  Tue,  2 May 2006 07:33:17 +0200

libterm-progressbar-perl (2.09-3) unstable; urgency=low

  * Orphaning this package as previously announced
  * Debhelper compatibility level 5
  * Bump Standards-Version
  * debian/control:
    + move debhelper to Build-Depends as it is used during clean
    + fix indentation of Homepage

 -- Florian Ernst <florian@debian.org>  Sun, 30 Apr 2006 12:08:17 +0200

libterm-progressbar-perl (2.09-2) unstable; urgency=low

  * New maintainer. (Closes: #331100: ITA: libterm-progressbar-perl --
    Perl module to print a progress bar)
  * debian/control: add upstream Homepage to long description
  * debian/copyright: extend note about previous and current maintainer
  * Minor adjustments to follow latest Perl Policy
  * Standards-Version 3.6.2, no changes required

 -- Florian Ernst <florian@debian.org>  Fri,  7 Oct 2005 16:19:09 +0200

libterm-progressbar-perl (2.09-1) unstable; urgency=low

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon, 14 Mar 2005 09:53:16 -0600

libterm-progressbar-perl (2.08-1) unstable; urgency=low

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 13 Mar 2005 12:49:32 -0600

libterm-progressbar-perl (2.07-1) unstable; urgency=low

  * New upstream release.
  * Bumped standards version to 3.6.1 (no packaging changes).

 -- Kenneth J. Pronovici <pronovic@debian.org>  Mon,  7 Mar 2005 10:27:03 -0600

libterm-progressbar-perl (2.06r1-1) unstable; urgency=low

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 21 Mar 2004 12:31:04 -0600

libterm-progressbar-perl (2.05-1) unstable; urgency=low

  * New upstream release.
  * Bumped standards version to 3.5.9.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed,  3 Sep 2003 12:38:56 -0500

libterm-progressbar-perl (2.04-1) unstable; urgency=low

  * New upstream release.
  * Added 'make test' to build process in debian/rules.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 17 Aug 2003 14:59:19 -0500

libterm-progressbar-perl (2.03-2) unstable; urgency=low

  * Changed maintainer address from @ieee.org to @debian.org.
  * Removed DH_COMPAT settting from debian/rules.
  * Added debian/compat file to replace DH_COMPAT setting.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sun, 30 Mar 2003 13:12:41 -0600

libterm-progressbar-perl (2.03-1) unstable; urgency=low

  * New upstream release.

 -- Kenneth J. Pronovici <pronovic@ieee.org>  Wed, 15 Jan 2003 00:06:10 -0600

libterm-progressbar-perl (2.02-1) unstable; urgency=low

  * New upstream release.
  * Removed "full stop" (i.e. ".") from debian/control Description line.

 -- Kenneth J. Pronovici <pronovic@ieee.org>  Mon, 30 Dec 2002 15:53:49 -0600

libterm-progressbar-perl (2.01-3) unstable; urgency=low

  * Added debian/watch to look for new package versions as they come out.

 -- Kenneth J. Pronovici <pronovic@ieee.org>  Mon, 30 Dec 2002 15:11:45 -0600

libterm-progressbar-perl (2.01-2) unstable; urgency=low

  * Changed debian/copyright to reference /usr/share/common-licenses.
    Corrected formatting in debian/control (max 2 spaces indent).

 -- Kenneth J. Pronovici <pronovic@ieee.org>  Sun,  3 Nov 2002 16:17:57 -0600

libterm-progressbar-perl (2.01-1) unstable; urgency=low

  * New upstream release, and also minor packaging clean-ups.

 -- Kenneth J. Pronovici <pronovic@ieee.org>  Tue, 29 Oct 2002 10:09:27 -0600

libterm-progressbar-perl (2.00-2) unstable; urgency=low

  * Debian packaging clean-up.

 -- Kenneth J. Pronovici <pronovic@ieee.org>  Sun,  6 Oct 2002 15:40:58 -0500

libterm-progressbar-perl (2.00-1) unstable; urgency=low

  * Initial Release.

 -- Kenneth J. Pronovici <pronovic@ieee.org>  Mon, 23 Sep 2002 18:51:41 -0500
